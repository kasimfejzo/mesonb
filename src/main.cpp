#include <iostream>
#include <string>

#include <country_info.h>

int main(int argc, char *argv[]) {
  auto logger = spdlog::basic_logger_mt("LOGGER", "log.txt");

  CountryInfo ci;
  while(1) {
    if(!ci.showProgram()) break;
  }   

  return 0;
}

