#include <country_info.h>
#include <fstream>

void log(const std::string&);

void CountryInfo::actionLog() {
  std::ifstream in("actionlog.txt");
  std::vector<std::string> actions;

  while(in.good()) {
    std::string temp;
    in >> temp;
    actions.push_back(temp);
  }

  if(actions.empty()) return;
  for(unsigned int i = 0; i < actions.size();) {
    if(i == actions.size() - 1) {
      getInfo(actions[i], false, "");
      ++i;
    }
    else if(i + 1 <= actions.size() && actions[i + 1] == "hasParam") {
      getInfo(actions[i], false, actions[i + 2]);
      i += 3; 
    }
    else {
      getInfo(actions[i], false, "");
      ++i;
    }
  }
  in.close();

  std::remove("actionlog.txt");
}

void CountryInfo::getInfo(std::string& option, bool hasParam, std::string name = "") {
  std::ofstream Out("actionlog.txt", std::ios::out | std::ios::app | std::ios::binary);
  if(hasParam) {
    std::cout << "Enter " + option << std::endl;
    std::cin >> name;

    Out << "hasParam " + name + " ";
  }

  auto r = cpr::Get(cpr::Url{url_ + option + "/" + name});
  log(r.text);
}

bool CountryInfo::showProgram() {
  if(first_) {
    actionLog();
    first_ = !first_;
  }
  
  std::cout << "Choose one: " << std::endl;
  std::cout << "1. By name" << std::endl;
  std::cout << "2. By currency" << std::endl;
  std::cout << "3. By language" << std::endl;
  std::cout << "4. By capital" << std::endl;
  std::cout << "5. By region" << std::endl;
  std::cout << "6. All" << std::endl;
  std::cout << "Any other number to exit" << std::endl;

  int option;
  std::cin >> option;

  std::string StrOption;
  bool hasParam;
  switch(option) {
    case 1:
      StrOption = "name";
      hasParam = true;
      break;
    case 2:
      StrOption = "currency";
      hasParam = true;
      break;
    case 3:
      StrOption = "lang";
      hasParam = true;
      break;
    case 4:
      StrOption = "capital";
      hasParam = true;
      break;
    case 5:
      StrOption = "region";
      hasParam = true;
      break;
    case 6:
      StrOption = "all";
      hasParam = false;
      break;
    default:
      return false;
  }

  std::ofstream out("actionlog.txt", std::ios::out | std::ios::app | std::ios::binary);
  out << StrOption + " ";
  out.close();
  
  getInfo(StrOption, hasParam);
  return true;
}



void log(const std::string& text) {
  try {
    spdlog::get("LOGGER") -> info(text);
    
    std::cout << "LOGGER" << std::endl;
    std::cout << text << std::endl; 
  }
  catch(const spdlog::spdlog_ex& ex) {
    std::cout << ex.what() << std::endl;
  }
}
