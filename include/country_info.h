#ifndef __COUNTRY_INFO_H_
#define __COUNTRY_INFO_H_

#include <string>
#include <iostream>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <cpr/cpr.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <cpr/cpr.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <cpr/cpr.h>

class CountryInfo {
private:
  void actionLog();
  void getInfo(std::string&, bool, std::string);

  std::string url_;
  bool first_;
public:
  CountryInfo() : url_{"https://restcountries.eu/rest/v2/"}, first_{true} {};

  bool showProgram();
};


#endif /* __COUNTRY_INFO_H_ */
